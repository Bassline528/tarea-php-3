<?php

// Declarar un vector de 20 elementos con valores aleatorios de 1 a 10.
// Crear una función recursiva en PHP que recorra el array de atrás para adelante. Se deberá
// imprimir desde el último elemento del array hasta el primero
// Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio.

function recorrerDesdeAtras($vector) 
 {
    $lenghtOfVector = count($vector);

    for ($i=$lenghtOfVector; $i >= 0; $i--) { 
        echo "$vector[$i] ";
        
    }

 }

 $lista = array(
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
    rand(1,10),
 );



 recorrerDesdeAtras($lista);


 
 