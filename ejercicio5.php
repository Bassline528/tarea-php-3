<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ejercicio 5</title>
</head>
<style>
</style>
<body>
<!-- 
Hacer un script PHP que genere un formulario HTML que contenga los siguientes campos: nombre,
apellido, edad y el botón de submit.
• Se deben usar las cadenas HEREDOC.
-->

    <?php 
        $form = <<<EOD
        <form>
            <input type="text" name="nombre" placeholder="nombre" />
            <input type="text" name="apellido" placeholder="apellido"/>
            <input type="text" name="edad" placeholder="edad"/>
            <input type="submit" name="submit" value="Enviar" />
        </form>    
        EOD;
        echo $form;
    ?>
    
  
</body>
</html>