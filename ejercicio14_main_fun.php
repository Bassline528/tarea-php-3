<?php

include "ejercicio14_string_fun.php";

function superMegaGenialFuncion()
{
    $strangeAssociativeArray = array(
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
        str_makerand() => rand(1, 1000),
    );
    
    $exist = false;
    foreach ($strangeAssociativeArray as $key => $value) {
        if(
            str_starts_with($key, "a") || 
            str_starts_with($key, "d") ||
            str_starts_with($key, "m") ||
            str_starts_with($key, "z") 
    
        ) {
            $exist = true;
            print "$key => $value \n" ;
        }
    }
    
    if ($exist == false) {
        print "no existe ningún índice que comience con esas letras";
    }

}