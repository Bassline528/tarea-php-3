<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ejercicio 3</title>
</head>
<style>
table, th, td {
  border:1px solid black;
}
</style>
<body>
<!-- • Generar un script PHP que cree una tabla HTML con los números pares que existen entre 1
y N.
• El número N estará definido por una constante PHP. El valor de la constante N debe ser un
número definido por el alumno.
El script PHP debe estar embebido en una página HTML. -->

    <table>
        <?php
            $n=20;
        ?>
        <tr>
            <th>Números pares que existen entre 1 y <?php echo $n ?></th>
        </tr>
        <tr>

        
        <?php
            for ($i=1; $i < $n ; $i++) { 
                if($i%2==0 ){
                    echo "<tr>";
                    echo "<td> $i </td>";
                    echo "</tr>";
                }
            }
        ?>
        </tr>
    </table>
</body>
</html>