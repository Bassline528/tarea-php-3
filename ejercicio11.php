<?php
// Realizar un script PHP que haga lo siguiente:
// Declare un vector indexado cuyos valores sean urls (esto simula un log de acceso de un usuario).
// El script debe contar e imprimir cuantas veces accedió el usuario al mismo url (no se deben repetir
// los url).

    $urls = array(
        array("url" => "https://www.google.com/","counter" => 0),
        array("url" => "https://www.gitlab.com/","counter" => 0),
        array("url" => "https://www.youtube.com/","counter" => 0),
        array("url" => "https://www.abc.com.py/","counter" => 0),
        array("url" => "https://www.reddit.com/","counter" => 0),
        array("url" => "https://www.twitter.com/","counter" => 0),
        array("url" => "https://www.github.com/","counter" => 0),
        array("url" => "https://www.facebook.com/","counter" => 0),
        array("url" => "https://www.instagram.com/","counter" => 0),
        array("url" => "https://www.steam.com/","counter" => 0),
        array("url" => "https://www.discord.com/","counter" => 0),
    );

    //TODO: no entendi que verga habia que hacer xd