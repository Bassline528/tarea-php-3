<?php
// Hacer un script PHP que haga lo siguiente:
// • Crear un array indexado con 10 cadenas.
// • Crear una variable que tenga un string cualquiera como string.
// El script debe:
// • Buscar si la cadena declarada está en el vector declarado.
// • Si esta, se imprime “Ya existe” la cadena.
// • Si no está, se imprime “Es Nuevo” y se agrega al Final del array.
// Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio.


function verificarSiPalabraExiste($word, $words)
{
    if(in_array($word, $words)) {
        echo "Ya existe";
    } else {
        echo "Es nuevo, se agrega al final";
        array_push($words, $word);
    }
    # code...
}

$words = ["lechuga", "papa", "messi", "increible", "javascript", "gato", "advertencia", "reintegración", "teleton", "pintura"];
    verificarSiPalabraExiste("chocolate", $words);