<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ejercicio 4</title>
</head>
<style>

    .max {
        color: green;
    }
    .min {
        color: red;
    }

</style>

<body>
<!-- 
Hacer un script PHP que haga lo siguiente:
• Declarar 3 variable con valores enteros aleatorios (se debe generar aleatoriamente estos
valores entre 50 y 900) – Se debe usar la función mt_srand y mt_rand.
• Ordenar de mayor a menor los valores de estas variables.
• Imprimir en pantalla la secuencia de números ordenadas, los números deben estar
separados por espacios en blanco.
• El número mayor debe estar en color verde y el número más pequeño debe estar en color
rojo.
-->
    <?php
        mt_srand(23536); //TODO: VOLVER A MIRAR ESTA FUNCION

        $var1 = mt_rand(50,900);
        $var2 = mt_rand(50,900);
        $var3 = mt_rand(50,900);

        $varsArray = [$var1, $var2, $var3];
        
        rsort($varsArray);

        $lenght = count($varsArray);
        
        echo "<div>";
        foreach ($varsArray as $key => $value) {

            switch ($key) {
                case 0:
                    echo "<span class='max'>$value </span>";
                    break;
                case $lenght - 1:
                    echo "<span class='min'>$value </span>";
                    break;
                default:
                    echo "<span>$value </span>";
                    break;
            }

        }
        echo "</div>";
        
    ?>

  
</body>
</html>