<?php
// Hacer un script en PHP que determine si una cadena dada es un palíndromo
// Un palíndromo (del griego palin dromein, volver a ir hacia atrás) es una palabra, número o frase
// que se lee igual hacia adelante que hacia atrás. Si se trata de un número, se llama capicúa.
// Habitualmente, las frases palindrómicas se resienten en su significado cuanto más largas son
// Ejemplos: radar, Neuquén, anilina, ananá, Malayalam
// Obs: El alumno deberá crear sus propias funciones para realizar este ejercicio.

function Palindrome($string){
    if ((strlen($string) == 1) || (strlen($string) == 0)){
        echo "Palindrome";
    }
    else{
        if (substr($string,0,1) == substr($string,(strlen($string) - 1),1)){
            return Palindrome(substr($string,1,strlen($string) -2));
        }
        else{
            echo " Not a Palindrome"; }
    }
}
 
$string = "RASAR";
Palindrome($string);