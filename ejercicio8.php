<?php

// Hacer un script en PHP que genere una matriz de dimensión n*m con números aleatorios. Las
// dimensiones n (filas) y m (columnas) son variables que debe definir el alumno. La matriz generada
// se debe imprimir en pantalla de manera tabular.
// Obsevación: El alumno deberá crear sus propias funciones para realizar este ejercicio.
    $filas = 5;
    $columnas = 7;
    
    function generarMatriz($n, $m)
    {
        $matriz = [];
    
        for ($i=0; $i < $n; $i++) { 
    
            $column = [];
    
            for ($j=0; $j < $m; $j++) { 
    
                $random = rand(1, 100);
                array_push($column, $random);
            }
    
            array_push($matriz, $column);
        }
        # code...
        echo "<pre>";
        print_r($matriz);
        echo "</pre>";
    }


    generarMatriz($filas, $columnas);
