
<!-- 2- Ejercicio 2:
Hacer un script PHP que imprime la siguiente información:
• Versión de PHP utilizada.
• El id de la versión de PHP.
• El valor máximo soportado para enteros para esa versión.
• Tamaño máximo del nombre de un archivo.
• Versión del Sistema Operativo.
• Símbolo correcto de 'Fin De Línea' para la plataforma en uso.
• El include path por defecto.
Observación: Ver las constantes predefinidas del núcleo de PHP -->


<?php
    $phpVersion = PHP_VERSION;
    $phpVersionId = PHP_VERSION_ID;
    $valorMaximoEnterosVersion = PHP_INT_MAX;
    $maximunNameSize = PHP_MAXPATHLEN;
    $osVersion = PHP_OS;
    $endOfLineSymbol = PHP_EOL;
    $defaultIncludePath  = DEFAULT_INCLUDE_PATH;

    echo "Versión de PHP utilizada: $phpVersion \r\n";
    echo "El id de la versión de PHP: $phpVersionId \r\n";
    echo "El valor máximo soportado para enteros para esa versión: $valorMaximoEnterosVersion \r\n";
    echo "Tamaño máximo del nombre de un archivo: $maximunNameSize \r\n";
    echo "Versión del Sistema Operativo: $osVersion \r\n";
    echo "Símbolo correcto de 'Fin De Línea' para la plataforma en uso: $endOfLineSymbol \r\n";
    echo "El include path por defecto:   $defaultIncludePath  \r\n";
        
