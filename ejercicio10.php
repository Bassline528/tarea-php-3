<?php

// Hacer un script PHP que declare un vector de 50 elementos con valores aleatorios enteros entre 1
// y 1000. El script debe determinar cuál es el elemento con mayor valor en el vector, se debe
// imprimir un mensaje con el valor y el índice en donde se encuentra.
// Mensaje de ejemplo: El elemento con índice 8 posee el mayor valor que 789.
// Obs: El alumno deberá crear sus propias funciones para realizar este ejercicio.

function superFuncionIncreible2(){
    $lenght = 50;
    
    $elements = [];
    for ($i=0; $i <  $lenght; $i++) { 
        $randomNumber = rand(1, 1000);
        array_push($elements, $randomNumber);
    }
    $maxValue = max($elements);
    $maxs = array_keys($elements, $maxValue);
    
    
    echo "<pre> El elemento con índice $maxs[0] posee el mayor valor que es $maxValue. </pre>";
    
}

superFuncionIncreible2();